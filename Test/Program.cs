﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechCoreLaboratories.AICore;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            NeuronNetwork neuronNetwork = new NeuronNetwork();
            neuronNetwork.Neurons.Add(new Neuron());

            Console.ReadLine();
        }
    }
}
