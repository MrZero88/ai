﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechCoreLaboratories.AICore
{
    public class Neuron
    {
        public List<Connection> Connections = new List<Connection>();
        public double Bias = 0;
        public double Value = 0;

        private void calculate() => Value = Math.Tanh(Connections.Select(c => c.getValue()).Sum() + Bias);

        public double getValue()
        {
            if(Value == 0) calculate();
            double value = Value;
            Value = 0;
            return value;
        }
    }
}
