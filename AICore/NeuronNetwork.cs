﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechCoreLaboratories.AICore
{
    public class NeuronNetwork
    {
        public List<Neuron> Neurons = new List<Neuron>();
        public List<Input> Inputs = new List<Input>();
        public List<Output> Outputs = new List<Output>();
        public List<Trainer> Trainers = new List<Trainer>();

        public void calculate()
        {
            foreach (Input input in Inputs)
                input.update();
            foreach (Output output in Outputs)
                output.calculate();
        }
    }
}
