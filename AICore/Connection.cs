﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechCoreLaboratories.AICore
{
    public class Connection
    {
        public Neuron neuron;
        private double weight = 1;

        public double getValue()
        {
            return neuron.getValue() + weight;
        }
    }
}
